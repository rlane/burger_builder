import * as actionTypes from '../actions/actionTypes';
import * as utilities from '../../shared/utility';

const BASE_PRICE = 4;

const initialState = {
  ingredients: null,
  totalPrice: BASE_PRICE,
  error: false,
  building: false
};

const INGREDIENT_PRICES = {
  salad: 0.5,
  cheese: 0.4,
  meat: 1.3,
  bacon: 0.7
}

const modifyIngredient = (action, value, state) => {
  const name = action.ingredientName;
  const ingredientValue = state.ingredients[name] + value;
  const updatedIngredients = utilities.updateObject(state.ingredients, { [name]: ingredientValue } );
  const stateUpdates = {
    ingredients: updatedIngredients,
    totalPrice: state.totalPrice + (INGREDIENT_PRICES[name] * value),
    building: true
  }
  return utilities.updateObject(state, stateUpdates)
}

const setIngredients = (action, state) => {
  const updatedState = {
    ingredients: action.ingredients,
    totalPrice: BASE_PRICE,
    error: false,
    building: false
  }
  return utilities.updateObject(state, updatedState);
}

const fetchIngredientsFailed = (action, state) => {
  const updatedState = {
    error: action.error
  };
  return utilities.updateObject(state, updatedState);
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.ADD_INGREDIENT: return modifyIngredient(action, 1, state);
    case actionTypes.REMOVE_INGREDIENT: return modifyIngredient(action, -1, state);
    case actionTypes.SET_INGREDIENTS: return setIngredients(action, state);
    case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(action, state);
    default: return state;
  }
}

export default reducer;
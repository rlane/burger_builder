import * as actionTypes from '../actions/actionTypes';
import * as utilities from '../../shared/utility';

const initialState = {
  orders: [],
  loading: false,
  purchased: false
};

const setLoading = (loading, state) => {
  const updatedState = {
    loading: loading
  }
  return utilities.updateObject(state, updatedState);
}

const purchaseBurgerInit = (state) => {
  const updatedState = {
    loading: false,
    purchased: false
  }
  return utilities.updateObject(state, updatedState);
}

const purchaseBurgerSuccess = (action, state) => {
  const newOrder = utilities.updateObject(action.orderData, { id: action.orderId } );
  const updatedState = {
    loading: false,
    purchased: true,
    orders: state.orders.concat(newOrder)
  };
  return utilities.updateObject(state, updatedState);
}

const fetchOrdersSuccess = (action, state) => {
  const updatedState = {
    orders: action.orders,
    loading: false
  }
  return utilities.updateObject(state, updatedState)
}

const fetchOrdersFail = (action, state) => {
  const updatedState = {
    error: action.error,
    loading: false
  };
  return utilities.updateObject(state, updatedState);
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PURCHASE_INIT: return purchaseBurgerInit(state);
    case actionTypes.PURCHASE_BURGER_START: return setLoading(true, state);
    case actionTypes.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(action, state);
    case actionTypes.PURCHASE_BURGER_FAIL: return setLoading(false, state);
    case actionTypes.FETCH_ORDERS_START: return setLoading(true, state);
    case actionTypes.FETCH_ORDERS_SUCCESS: return fetchOrdersSuccess(action, state);
    case actionTypes.FETCH_ORDERS_FAIL: return fetchOrdersFail(action, state);
    default: return state;
  }
};

export default reducer;
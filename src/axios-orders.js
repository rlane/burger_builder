import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-builder-56642.firebaseio.com/'
});

export default instance;
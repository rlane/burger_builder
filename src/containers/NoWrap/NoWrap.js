const noWrap = (props) => props.children;

export default noWrap;